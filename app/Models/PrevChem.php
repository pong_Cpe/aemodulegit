<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrevChem extends Model
{
    use HasFactory;

    protected $table = 'prev_chem';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'broker_id', 'chem_package_id', 'value'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function chempackage()
    {
        return $this->hasOne('App\Models\ChemPackage', 'id', 'chem_package_id');
    }
}
