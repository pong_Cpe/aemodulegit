@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Crop Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('crops.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('View Crop') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">

                            <tr>
                                <th>Name</th>
                                <td>{{ $crop->name }}</td>
                            </tr>
                            <tr>
                                <th>Details</th>
                                <td>{{ $crop->details }}</td>
                            </tr>
                            <tr>
                                <th>Sap Code</th>
                                <td>{{ $crop->sap_code }}</td>
                            </tr>
                            <tr>
                                <th>Link Url</th>
                                <td>{{ $crop->linkurl }}</td>
                            </tr>

                            <tr>
                                <th>Max Per Day</th>
                                <td>{{ $crop->max_per_day }}</td>
                            </tr>
                            <tr>
                                <th>Start Date</th>
                                <td>{{ $crop->startdate }}</td>
                            </tr>

                            <tr>
                                <th>End Date</th>
                                <td>{{ $crop->enddate }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('crops.edit', $crop->id) }}">Edit</a>
                                    <form action="{{ route('crops.destroy', $crop->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
