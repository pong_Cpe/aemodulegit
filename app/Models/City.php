<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $table = 'cities';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['province_id', 'th_name', 'en_name'];

    public function province()
    {
        return $this->hasOne('App\Models\Provice', 'id', 'province_id');
    }
}
