<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    protected $table = 'provinces';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['th_name', 'en_name'];
}
