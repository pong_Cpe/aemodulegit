<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HarvestMoveQ extends Model
{
    use HasFactory;

    protected $table = 'harvest_move_q';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'harvest_plan_id', 'sowing_id', 'type', 'to_date', 'from_date', 'age', 'status', 
        'mat_type', 'havest_by', 'value_est', 'value_bf_harvest', 'value_act', 'note', 
        'createdBy', 'modifiedBy'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }

    public function harvestplan()
    {
        return $this->hasOne('App\Models\HarvestPlan', 'id', 'harvest_plan_id');
    }

    public function createby()
    {
        return $this->hasOne('App\Models\User', 'id', 'createdBy');
    }

    public function updateby()
    {
        return $this->hasOne('App\Models\User', 'id', 'modifiedBy');
    }

}
