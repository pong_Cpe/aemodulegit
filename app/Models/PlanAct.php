<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanAct extends Model
{
    use HasFactory;

    protected $table = 'plan_acts';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'sowing_id', 'plan_schedule_id', 'plan_code', 'plan_date', 're_plan_date', 
        'act_date', 'spray_date', 'note', 'createdBy', 'modifiedBy', 
        'farmer_id', 'priority', 'age', 'sample_status'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }

    public function planschedule()
    {
        return $this->hasOne('App\Models\PlanSchedule', 'id', 'plan_schedule_id');
    }

    public function farmer()
    {
        return $this->hasOne('App\Models\Farmer', 'id', 'farmer_id');
    }

    public function planactdetails()
    {
        return $this->hasMany('App\Models\PlanActDetail', 'plan_act_id');
    }

}
