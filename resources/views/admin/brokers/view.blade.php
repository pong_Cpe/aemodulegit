@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Broker Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('brokers.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('View Broker') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">

                            <tr>
                                <th>Code</th>
                                <td>{{ $broker->code }}</td>
                            </tr>
                            <tr>
                                <th>ชื่อ นามสกุล</th>
                                <td>{{ $broker->init }} {{ $broker->fname }} {{ $broker->lname }}</td>
                            </tr>
                            <tr>
                                <th>เลขบัตรประชาชน</th>
                                <td>{{ $broker->citizenid }}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>{{ $broker->sub_cities }} {{ $broker->address1 }} {{ $broker->address2 }}
                                    {{ $broker->address3 }}<br>
                                    {{ $broker->city->th_name }} {{ $broker->province->th_name }}</td>
                            </tr>
                            <tr>
                                <th>Broker Color</th>
                                <td><span
                                        style="background-color: {{ $broker->broker_color }} !important;">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </td>
                            </tr>

                            <tr>
                                <th>Loc</th>
                                <td>{{ $broker->loc }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('brokers.edit', $broker->id) }}">Edit</a>
                                    <form action="{{ route('brokers.destroy', $broker->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
