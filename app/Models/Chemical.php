<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chemical extends Model
{
    use HasFactory;

    protected $table = 'chemicals';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['code', 'name', 'details', 'formula_code', 'standard_code_id', 'unit_id', 'rate_per_land', 'bigunit_id', 'package_per_bigunit', 'ctype'];
    
}
