<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmpSchedule extends Model
{
    use HasFactory;

    protected $table = 'tmp_schedules';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'name', 'details', 'status', 'broker_id'
    ];

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function tmpscheduleplans()
    {
        return $this->hasMany('App\Models\TmpSchedulePlan', 'tmp_schedule_id');
    }
}
