<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceiveMatDetail extends Model
{
    use HasFactory;

    protected $table = 'receive_mat_details';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'receive_mat_id', 'harvest_plan_id', 'sowing_id', 'input_item_id', 
        'qty', 'unit_id', 'real_qty', 'real_unit_id', 'start_weight', 'last_weight', 
        'act_weight', 'diff_weight', 'created_by', 'modified_by', 'created', 'modified', 
        'mat_code_id', 'diff', 'receive_mat_group_id', 'sub_input_item_id'];

    public function receivemat()
    {
        return $this->hasOne('App\Models\ReceiveMat', 'id', 'receive_mat_id');
    }

    public function harvestplan()
    {
        return $this->hasOne('App\Models\HarvestPlan', 'id', 'harvest_plan_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function unit()
    {
        return $this->hasOne('App\Models\Unit', 'id', '');
    }

    public function realunit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'real_unit_id');
    }

    public function matcode()
    {
        return $this->hasOne('App\Models\MatCode', 'id', 'mat_code_id');
    }

    public function receivematgroup()
    {
        return $this->hasOne('App\Models\ReceiveMatGroup', 'id', 'receive_mat_group_id');
    }

    public function subinputitem()
    {
        return $this->hasOne('App\Models\SubInputItem', 'id', 'sub_input_item_id');
    }
        
}
