<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SamplePlan extends Model
{
    use HasFactory;

    protected $table = 'sample_plans';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'area_id', 'broker_id', 'farmer_id', 'sowing_id', 'input_item_id', 
        'user_id', 'head_id', 'seed_code_id', 'age', 'harvest_type', 'havest_by', 
        'ref_id', 'mat_type', 'date_plan', 'date_harvest', 'date_est', 'date_act', 
        'land_value', 'value_est', 'value_bf_harvest', 'value_act', 'marked', 'sample_qa', 
        'sample_qc', 'qa_value', 'qc_value', 'qa_recv', 'qc_recv', 'qc_result_value', 'qc_result_txt', 
        'qa_status', 'qc_status', 'note', 'createdBy', 'modifiedBy'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function area()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function farmer()
    {
        return $this->hasOne('App\Models\Farmer', 'id', 'farmer_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function head()
    {
        return $this->hasOne('App\Models\Head', 'id', 'head_id');
    }

    public function seedcode()
    {
        return $this->hasOne('App\Models\SeedCode', 'id', 'seed_code_id');
    }
}
