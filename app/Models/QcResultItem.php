<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QcResultItem extends Model
{
    use HasFactory;

    protected $table = 'qc_result_items';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'qc_result_master_id', 'qc_defect_id', 'weight_df_unit_id', 'weight_df'
    ];

    public function qcresultmaster()
    {
        return $this->hasOne('App\Models\QcResultMaster', 'id', 'qc_result_master_id');
    }

    public function qcdefect()
    {
        return $this->hasOne('App\Models\QcDefect', 'id', 'qc_defect_id');
    }

    public function dfweightunit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'weight_df_unit_id');
    }
}
