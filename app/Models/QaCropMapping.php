<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaCropMapping extends Model
{
    use HasFactory;

    protected $table = 'qa_crop_mappings';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['crop_id', 'input_item_id', 'qa_rate_set_id', 'createdBy', 'modifiedBy'];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function qarateset()
    {
        return $this->hasOne('App\Models\QaRateSet', 'id', 'qa_rate_set_id');
    }
}
