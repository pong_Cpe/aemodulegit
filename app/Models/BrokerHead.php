<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrokerHead extends Model
{
    use HasFactory;

    protected $table = 'broker_head';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['crop_id', 'broker_id', 'head_id', 'createdBy', 'modifiedBy'];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function head()
    {
        return $this->hasOne('App\Models\Head', 'id', 'head_id');
    }

}
