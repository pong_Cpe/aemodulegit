<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaItemDetail extends Model
{
    use HasFactory;

    protected $table = 'qa_item_details';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'qa_master_id', 'qa_rate_chem_id', 'rate',, 'standard', 'note', 'status', 'createdBy', 'modifiedBy'
    ];

    public function qamaster()
    {
        return $this->hasOne('App\Models\QaMaster', 'id', 'qa_master_id');
    }

    public function qaratechem()
    {
        return $this->hasOne('App\Models\QaRateChem', 'id', 'qa_rate_chem_id');
    }
}
