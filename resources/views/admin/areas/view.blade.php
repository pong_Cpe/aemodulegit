@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Area Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('areas.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('View Area') }}</div>

                    <div class="card-body">

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Code</th>
                                <td>{{ $area->code }}</td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>{{ $area->name }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('areas.edit', $area->id) }}">Edit</a>
                                    <form action="{{ route('areas.destroy', $area->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
