<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SowingDetail extends Model
{
    use HasFactory;

    protected $table = 'sowing_details';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'sowing_id', 'tran_date', 'input_item_id', 'item_value', 'land_size', 
        'gps_land', 'gps_seed', 'details', 'status', 'createdBy', 'modifiedBy', 
        'seed_code_id', 'case_type'
    ];

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function seedcode()
    {
        return $this->hasOne('App\Models\SeedCode', 'id', 'seed_code_id');
    }
}
