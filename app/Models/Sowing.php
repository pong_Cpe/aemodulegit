<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sowing extends Model
{
    use HasFactory;

    protected $table = 'sowings';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['crop_id','farmer_id','input_item_id','harvest_type_id','land_size',
    'item_value','gps_land','gps_seed','status',
    'grow_rate','grow_date','grow_note','start_date','details','relate','n_pos','s_pos','e_pos','w_pos',
    'createdBy','modifiedBy','area_id',
    'prev1','prev2','prev3','head_id','user_id','current_land','fail_land','fail_date',
    'fail_condition','yield_rate','name','amg_land','ph_water',
    'ph_soy','land_status',
    'seed_code_id','seed_pack_id',
    'harvest_status',
    'seed_code_text','seed_code_date','farmer_status','raw_json','yield_rate7',
    'lat','lng','qa_status','qc_status','user_farmer_id','a2seed','a3seed','f_land',
    'post_harvest_complain','post_harvest_qc','post_harvest_qa',
    'post_harvest_er','harvest_stage','check_current_date','n_pos_current',
    's_pos_current','e_pos_current','w_pos_current','land_type'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function farmer()
    {
        return $this->hasOne('App\Models\Farmer', 'id', 'farmer_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }


    public function harvesttype()
    {
        return $this->hasOne('App\Models\HarvestType', 'id', 'harvest_type_id');
    }

    public function area()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function head()
    {
        return $this->hasOne('App\Models\Head', 'id', 'head_id');
    }

    public function Ae()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function npos()
    {
        return $this->hasOne('App\Models\PlantCode', 'id', 'n_pos');
    }

    public function spos()
    {
        return $this->hasOne('App\Models\PlantCode', 'id', 's_pos');
    }

    public function epos()
    {
        return $this->hasOne('App\Models\PlantCode', 'id', 'e_pos');
    }

    public function wpos()
    {
        return $this->hasOne('App\Models\PlantCode', 'id', 'w_pos');
    }

    public function prev1code()
    {
        return $this->hasOne('App\Models\PlantCode', 'id', 'prev1');
    }

    public function seedcode()
    {
        return $this->hasOne('App\Models\SeedCode', 'id', 'seed_code_id');
    }

    public function seedpack()
    {
        return $this->hasOne('App\Models\SeedPack', 'id', 'seed_pack_id');
    }

    public function nposcurrent()
    {
        return $this->hasOne('App\Models\PlantCode', 'id', 'n_pos_current');
    }
    public function sposcurrent()
    {
        return $this->hasOne('App\Models\PlantCode', 'id', 's_pos_current');
    }

    public function eposcurrent()
    {
        return $this->hasOne('App\Models\PlantCode', 'id', 'e_pos_current');
    }

    public function wposcurrent()
    {
        return $this->hasOne('App\Models\PlantCode', 'id', 'w_pos_current');
    }

    public function userfarmer()
    {
        return $this->hasOne('App\Models\UserFarmer', 'id', 'user_farmer_id');
    }

}
