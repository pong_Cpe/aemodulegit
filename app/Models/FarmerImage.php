<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FarmerImage extends Model
{
    use HasFactory;

    protected $table = 'farmer_images';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id','farmer_id', 'attach_dir', 'attach'
    ];

    public function farmer()
    {
        return $this->hasOne('App\Models\Farmer', 'id', 'farmer_id');
    }

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }
}
