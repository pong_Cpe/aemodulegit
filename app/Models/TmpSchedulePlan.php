<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmpSchedulePlan extends Model
{
    use HasFactory;

    protected $table = 'tmp_schedule_plans';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'tmp_schedule_id', 'input_item_id', 'code', 'main_code', 'name', 'details', 'day', 'priority'
    ];

    public function tmpschedule()
    {
        return $this->hasOne('App\Models\TmpSchedule', 'id', 'tmp_schedule_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function tmpscheduleplandetails()
    {
        return $this->hasMany('App\Models\TmpSchedulePlanDetail', 'tmp_schedule_plan_id');
    }
}
