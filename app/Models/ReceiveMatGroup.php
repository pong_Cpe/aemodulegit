<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceiveMatGroup extends Model
{
    use HasFactory;

    protected $table = 'receive_mat_groups';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'user_farmer_id', 'group_date', 'name', 'note', 'status', 'createdBy', 'modifiedBy'
    ];

    public function userfarmer()
    {
        return $this->hasOne('App\Models\UserFarmer', 'id', 'user_farmer_id');
    }
}
