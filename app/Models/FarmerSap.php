<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FarmerSap extends Model
{
    use HasFactory;
    protected $table = 'farmer_saps';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['farmer_id','sap_farmer_id','file_name'];

    public function farmer()
    {
        return $this->hasOne('App\Models\Farmer', 'id', 'farmer_id');
    }
}
