<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeedCode extends Model
{
    use HasFactory;

    protected $table = 'seed_codes';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['crop_id'
      ,'input_item_id'
      ,'code'
      ,'details'
      ,'val_per_area'
      ,'seed_per_kg'
      ,'pack_date'];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }
}
