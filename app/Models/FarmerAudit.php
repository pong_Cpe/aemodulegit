<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FarmerAudit extends Model
{
    use HasFactory;

    protected $table = 'farmer_audits';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'broker_id', 'user_farmer_id', 'sowing_id', 'user_id', 'audit_date1', 'audit_age1',
        'audit_date2', 'audit_age2', 'audit1_1_point', 'audit1_1_note', 'audit1_2_point', 'audit1_2_note', 'audit1_3_point',
        'audit1_3_note', 'audit2_4_point', 'audit2_4_note', 'audit2_5_point', 'audit2_5_note', 'audit2_6_point', 'audit2_6_note',
        'audit2_7_point', 'audit2_7_note', 'audit2_8_point', 'audit2_8_note', 'audit3_9_point', 'audit3_9_note', 'audit3_10_point',
        'audit3_10_note', 'audit3_11_point', 'audit3_11_note', 'total_point', 'grade'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function userfamer()
    {
        return $this->hasOne('App\Models\UserFarmer', 'id', 'user_farmer_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }
}
