<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcessCode extends Model
{
    use HasFactory;

    protected $table = 'process_codes';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'code', 'name',
        'note', 'created_by', 'modified_by'
    ];
}
