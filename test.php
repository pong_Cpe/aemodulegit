<?php

echo 'Blowfish: ',
crypt('rasmuslerdorf', '$2a$07$usesomesillystringforsalt$'),
"\n";

echo 'Blowfish: ',
_crypt('1111', '$2a$10$8/1zQmB9wjRHV.lRrCGE8OV/rP7vvMxub1uDGVRrvN5nwkjWpXeCS'),
"\n";


echo "\n $2a$10$8/1zQmB9wjRHV.lRrCGE8OV/rP7vvMxub1uDGVRrvN5nwkjWpXeCS";

$pass =  '11111';

$pass_crypt = '$2a$10$8/1zQmB9wjRHV.lRrCGE8OV/rP7vvMxub1uDGVRrvN5nwkjWpXeCS';

if ($pass_crypt == _crypt($pass, $pass_crypt)) {
  echo "Success! Valid password";
} else {
  echo "Invalid password";
}


 function _crypt($password, $salt = false) {
		if ($salt === false || $salt === null || $salt === '') {
			$salt = _salt(22);
			$salt = vsprintf('$2a$%02d$%s', array('10', $salt));
		}
        echo "Salt ". $salt;
		$invalidCipher = (
			strpos($salt, '$2y$') !== 0 &&
			strpos($salt, '$2x$') !== 0 &&
			strpos($salt, '$2a$') !== 0
		);
    echo " invalidCipher " . $invalidCipher;

    echo " Strlen ". strlen($salt)." ";
		if ($salt === true || $invalidCipher || strlen($salt) < 29) {
			return '';
		}
		return crypt($password, $salt);
	}

function _salt($length = 22)
{
    $salt = str_replace(
        array('+', '='),
        '.',
        base64_encode(sha1(uniqid('e73e8f26f12b4cc09a21e24d1bacb2e8e84db785', true), true))
    );
    return substr($salt, 0, $length);
}