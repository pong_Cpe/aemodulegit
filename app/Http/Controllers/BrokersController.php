<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Broker;
use App\Models\City;
use App\Models\Province;
use App\Models\BrokerHead;

class BrokersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 25;
        $brokers = Broker::latest()->paginate($perPage);

        return view('admin.brokers.index', compact('brokers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $citylist = City::pluck('th_name','id');
        $provincelist = Province::pluck('th_name', 'id');
        return view('admin.brokers.create',compact('citylist', 'provincelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        Broker::create($requestData);

        return redirect('brokers')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $broker = Broker::findOrFail($id);
        return view('admin.brokers.view', compact('broker'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $broker = Broker::findOrFail($id);
        $citylist = City::pluck('th_name', 'id');
        $provincelist = Province::pluck('th_name', 'id');
        return view('admin.brokers.edit', compact('broker', 'citylist', 'provincelist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $broker = Broker::findOrFail($id);
        $broker->update($requestData);

        return redirect('brokers')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Broker::destroy($id);

        return redirect('brokers')->with('flash_message', ' deleted!');
    }

    public function getHeadList(Request $request , $crop_id)
    {
        $broker_id = $request->get('broker_id');
        $dataR = BrokerHead::where('crop_id', $crop_id)
        ->where('broker_id', $broker_id)
        ->get();

        $data = array();
        foreach ($dataR as $dataRObj) {
            if($dataRObj->head->fname != '' &&
            $dataRObj->head->lname != ''){
                $data[$dataRObj->head_id] = $dataRObj->head->fname . " " . $dataRObj->head->lname;
            }
        }

        $jsondata = array(
            'data' => $data
        );
        
        echo json_encode($jsondata);
    }
}
