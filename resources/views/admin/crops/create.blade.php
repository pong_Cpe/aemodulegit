@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Crop Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('crops.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Create New Crop') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('crops.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="details"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Details') }}</label>

                                <div class="col-md-6">
                                    {!! Form::textarea('details', old('details'), ['class' => 'form-control', 'name' => 'name', 'required' => 'required', 'autocomplete' => 'name', 'autofocus' => 'autofocus']) !!}
                                    @error('details')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sap_code"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Sap Code') }}</label>

                                <div class="col-md-6">
                                    <input id="sap_code" type="text"
                                        class="form-control @error('sap_code') is-invalid @enderror" name="sap_code"
                                        value="{{ old('sap_code') }}" required autocomplete="sap_code" autofocus>

                                    @error('sap_code')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="linkurl"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Link Url') }}</label>

                                <div class="col-md-6">
                                    <input id="linkurl" type="text"
                                        class="form-control @error('linkurl') is-invalid @enderror" name="linkurl"
                                        value="{{ old('linkurl') }}" required autocomplete="linkurl" autofocus>

                                    @error('linkurl')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="max_per_day"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Max Per Day') }}</label>

                                <div class="col-md-6">
                                    <input id="max_per_day" type="number"
                                        class="form-control @error('max_per_day') is-invalid @enderror" name="max_per_day"
                                        value="{{ old('max_per_day') }}" required autocomplete="max_per_day" autofocus>

                                    @error('max_per_day')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="startdate"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Start Date') }}</label>

                                <div class="col-md-6">
                                    {!! Form::date('startdate', null, old('startdate'), ['class' => 'form-control', 'name' => 'startdate', 'required' => 'required', 'autocomplete' => 'startdate', 'autofocus' => 'autofocus']) !!}

                                    @error('startdate')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="enddate"
                                    class="col-md-4 col-form-label text-md-right">{{ __('End Date') }}</label>

                                <div class="col-md-6">
                                    {!! Form::date('enddate', null, old('enddate'), ['class' => 'form-control', 'name' => 'enddate', 'required' => 'required', 'autocomplete' => 'enddate', 'autofocus' => 'autofocus']) !!}

                                    @error('enddate')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
