<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Broker extends Model
{
    use HasFactory;

    protected $table = 'brokers';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['code', 'init', 'fname', 'lname', 'citizenid', 'address1', 'address2', 'address3', 
    'sub_cities', 'city_id', 'province_id', 'loc', 'broker_color', 'createdBy', 'modifiedBy'];

    public function city()
    {
        return $this->hasOne('App\Models\City', 'id', 'city_id');
    }

    public function province()
    {
        return $this->hasOne('App\Models\Province', 'id', 'province_id');
    }
    
    public function brokerareas()
    {
        return $this->hasMany('App\Models\BrokerArea', 'broker_id');
    }
    

}
