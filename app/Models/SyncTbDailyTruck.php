<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SyncTbDailyTruck extends Model
{
    use HasFactory;

    protected $table = 'syncTbDailyTruck';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 
        'tran_no', 'rcv_no', 'broker', 'b_name', 'car_idf', 'rcv_datetime', 
        'weight_in', 'memo', 'tran_no_type', 'bill_close', 'not_use', 'not_use_because', 
        'Personal_ID', 'Name', 'StatusNow', 'rcv_no2', 'num_Order', 'farmer_id', 'f_name', 
        'stkcode', 'stkdesc', 'qultity', 'pack_id', 'pack_short', 'pack_weight', 'kgd_bf', 
        'kgd_af', 'kgd_s', 'kgd_r', 'kgd_b', 'qul_u', 'kgd_u', 'Personal_ID2', 'Name2', 'not_use2', 
        'dfmcode', 'dfstepcode', 'sowing_date', 'harvest_age', 'recv_date', 
        'sowing_id', 'input_item_id', 
        'mat_type', 'harvest_by'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }
}
