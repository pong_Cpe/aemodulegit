@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Broker Management') }}</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-header">
                <a class="btn btn-success" href="{{ route('brokers.create') }}"> Create New Broker</a>
            </div>

            <div class="card-body">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Color / Loc</th>
                        <th></th>
                    </tr>
                    @foreach ($brokers as $broker)
                        <tr>
                            <td>{{ $broker->code }}</td>
                            <td>{{ $broker->init }} {{ $broker->fname }} {{ $broker->lname }}</td>
                            <td>{{ $broker->sub_cities }} {{ $broker->address1 }} {{ $broker->address2 }}
                                {{ $broker->address3 }}<br>
                                {{ $broker->city->th_name }} {{ $broker->province->th_name }}</td>
                            <td><span
                                    style="background-color: {{ $broker->broker_color }} !important;">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                {{ $broker->loc }}</td>
                            <td>
                                <a class="btn btn-info" href="{{ route('brokers.show', $broker->id) }}">Show</a>
                                <a class="btn btn-primary" href="{{ route('brokers.edit', $broker->id) }}">Edit</a>
                                <form action="{{ route('brokers.destroy', $broker->id) }}" method="POST">

                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection
