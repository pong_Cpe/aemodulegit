<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LandName extends Model
{
    use HasFactory;

    protected $table = 'land_names';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'broker_id', 'name'
    ];

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }
}
