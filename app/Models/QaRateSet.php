<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaRateSet extends Model
{
    use HasFactory;

    protected $table = 'qa_rate_sets';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'name', 'default_flag', 'clone_id', 'createdBy', 'modifiedBy'
    ];

    public function qaratechems()
    {
        return $this->hasMany('App\Models\QaRateChems', 'qa_rate_set_id');
    }

    public function qaregionmappings()
    {
        return $this->hasMany('App\Models\QaRegionMapping', 'qa_rate_set_id');
    }

}
