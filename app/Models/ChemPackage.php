<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChemPackage extends Model
{
    use HasFactory;

    protected $table = 'chem_package';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['crop_id','chemical_id','unit_id','small_unit','package_name','mat_code','details','package_in_big_unit'];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function chemical()
    {
        return $this->hasOne('App\Models\Chemical', 'id', 'chemical_id');
    }

    public function unit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'unit_id');
    }

    public function smallunit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'small_unit');
    }
}
