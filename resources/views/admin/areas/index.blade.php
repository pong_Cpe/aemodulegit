@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Area Management') }}</h3>
    </div>
    <div class="page-content">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><a class="btn btn-success" href="{{ route('areas.create') }}"> Create New
                            Area</a></div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                            @foreach ($areas as $area)
                                <tr>
                                    <td>{{ $area->code }}</td>
                                    <td>{{ $area->name }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('areas.show', $area->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('areas.edit', $area->id) }}">Edit</a>
                                        <form action="{{ route('areas.destroy', $area->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
