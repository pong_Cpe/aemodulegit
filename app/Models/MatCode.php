<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatCode extends Model
{
    use HasFactory;

    protected $table = 'mat_codes';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'input_item_id', 'sub_input_item_id', 'process_code_id', 'code', 'name', 'detail', 'status', 'created_by', 'modified_by'
    ];
}
