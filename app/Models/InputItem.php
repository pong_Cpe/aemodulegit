<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InputItem extends Model
{
    use HasFactory;
    protected $table = 'input_items';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['name','code','tradename','common_name','size','unit_id','pur_of_use','RM_Group'];

}
