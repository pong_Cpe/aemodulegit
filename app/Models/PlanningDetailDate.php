<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanningDetailDate extends Model
{
    use HasFactory;

    protected $table = 'planning_detail_date';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'version', 'planning_detail_id', 'plan_date', 'plan_harvest', 'age', 
        'yeild', 'plan_value', 'status', 'createdBy', 'modifiedBy', 
        'yeild_ae'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function planningdetail()
    {
        return $this->hasOne('App\Models\PlanningDetail', 'id', 'planning_detail_id');
    }
}
