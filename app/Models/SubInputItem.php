<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubInputItem extends Model
{
    use HasFactory;

    protected $table = 'standard_code';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'input_item_id', 'code', 'name', 'mat_code_set', 'note', 'created_by', 'modified_by'];

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }
}
