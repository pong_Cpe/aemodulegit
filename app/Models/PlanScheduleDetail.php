<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanScheduleDetail extends Model
{
    use HasFactory;

    protected $table = 'plan_schedule_details';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'plan_schedule_id', 'chemical_id', 'value', 'unit_id', 'p_value', 
        'p_unit_id', 'name', 'rate', 'ctype', 'set_group'
    ];

    public function planschedule()
    {
        return $this->hasOne('App\Models\PlanSchedule', 'id', 'plan_schedule_id');
    }

    public function chemical()
    {
        return $this->hasOne('App\Models\Chemical', 'id', 'chemical_id');
    }

    public function unit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'unit_id');
    }

    public function picunit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'p_unit_id');
    }
}
