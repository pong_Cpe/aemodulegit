<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlantCode extends Model
{
    use HasFactory;

    protected $table = 'plant_code';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'code', 'name', 'details'
    ];
        
}
