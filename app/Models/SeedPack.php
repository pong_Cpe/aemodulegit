<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeedPack extends Model
{
    use HasFactory;

    protected $table = 'seed_packs';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'seed_code_id', 'name', 'details', 'pack_date', 'status'
    ];

    public function seedcode()
    {
        return $this->hasOne('App\Models\SeedCode', 'id', 'seed_code_id');
    }
}
