<?php

use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserloginsController;
use App\Http\Controllers\GroupsController;
use App\Http\Controllers\AreasController;
use App\Http\Controllers\BrokersController;
use App\Http\Controllers\CropsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
    
});

Route::resource('userlogins', UserloginsController::class);
Route::resource('groups', GroupsController::class);
Route::resource('areas', AreasController::class);
Route::resource('brokers', BrokersController::class);
Route::get('brokers/getheadlist/{crop_id}', [BrokersController::class, 'getHeadList']);

Route::resource('crops', CropsController::class);

Route::get('/test',[TestController::class, 'index']);