<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QcDefect extends Model
{
    use HasFactory;

    protected $table = 'qc_defects';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'input_item_id', 'sub_input_item_id', 'process_code_id', 'name', 'details', 'set_group', 'normal_flag', 'cri_flag', 'status', 'created_by', 'modified_by'
    ];

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function subinputitem()
    {
        return $this->hasOne('App\Models\SubInputItem', 'id', 'sub_input_item_id');
    }

    public function processcode()
    {
        return $this->hasOne('App\Models\ProcessCode', 'id', 'process_code_id');
    }

}
