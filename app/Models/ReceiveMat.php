<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceiveMat extends Model
{
    use HasFactory;

    protected $table = 'receive_mats';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'broker_id', 'car_code', 'receive_date', 'transfer_time', 'receive_time', 
        'harvesting_date', 'receive_code', 'harvest_type_code', 'qa_sample_no', 'qc_sample_no', 
        'tent', 'tie_basket', 'air_tube', 'washing', 'washing_condition', 'status', 'created_by', 
        'modified_by', 'start_weight'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function receivematdetails()
    {
        return $this->hasMany('App\Models\ReceiveMatDetail', 'receive_mat_id');
    }
}
