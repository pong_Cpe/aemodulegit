@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Group Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('groups.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('View Group') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{ $group->name }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('groups.edit', $group->id) }}">Edit</a>
                                    <form action="{{ route('groups.destroy', $group->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
