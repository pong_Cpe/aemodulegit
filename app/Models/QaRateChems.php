<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaRateChems extends Model
{
    use HasFactory;

    protected $table = 'qa_rate_chems';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'qa_rate_set_id', 'chemical_name', 'method', 'lod', 'mrl', 'custom1', 'custom2', 'custom3', 'custom4', 'custom5', 'custom6', 'custom7', 'custom8', 'custom9', 'custom10', 'custom11', 'custom12', 'custom13', 'custom14', 'custom15', 'custom16', 'custom17', 'custom18', 'custom19', 'custom20', 'note', 'createdBy', 'modifiedBy'];

    public function qarateset()
    {
        return $this->hasOne('App\Models\QaRateSet', 'id', 'qa_rate_set_id');
    }
}
