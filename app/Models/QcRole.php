<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QcRole extends Model
{
    use HasFactory;

    protected $table = 'qc_roles';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'mat_code_id', 'name', 'details', 'status', 'created_by', 'modified_by', 'help_other', 'help_cri'
    ];

    public function matcode()
    {
        return $this->hasOne('App\Models\MatCode', 'id', 'mat_code_id');
    }
}
