@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Broker Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('brokers.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Create New Broker') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('brokers.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="code" class="col-md-4 col-form-label text-md-right">{{ __('Code') }}</label>

                                <div class="col-md-6">
                                    <input id="code" type="text" class="form-control @error('code') is-invalid @enderror"
                                        name="code" value="{{ old('code') }}" required autocomplete="code" autofocus>

                                    @error('code')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="init"
                                    class="col-md-4 col-form-label text-md-right">{{ __('คำนำหน้า') }}</label>

                                <div class="col-md-6">
                                    <input id="init" type="text" class="form-control @error('init') is-invalid @enderror"
                                        name="init" value="{{ old('init') }}" autocomplete="init">

                                    @error('init')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname"
                                    class="col-md-4 col-form-label text-md-right">{{ __('ชื่อ') }}</label>

                                <div class="col-md-6">
                                    <input id="fname" type="text" class="form-control @error('fname') is-invalid @enderror"
                                        name="fname" value="{{ old('fname') }}">

                                    @error('fname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname"
                                    class="col-md-4 col-form-label text-md-right">{{ __('นามสกุล') }}</label>

                                <div class="col-md-6">
                                    <input id="lname" type="text" class="form-control @error('lname') is-invalid @enderror"
                                        name="lname" value="{{ old('lname') }}" required>

                                    @error('lname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="citizenid"
                                    class="col-md-4 col-form-label text-md-right">{{ __('เลขบัตรประชาชน') }}</label>

                                <div class="col-md-6">
                                    <input id="citizenid" type="text"
                                        class="form-control @error('citizenid') is-invalid @enderror" name="citizenid"
                                        value="{{ old('citizenid') }}" required>

                                    @error('citizenid')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address1"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Address1') }}</label>

                                <div class="col-md-6">
                                    <input id="address1" type="text"
                                        class="form-control @error('address1') is-invalid @enderror" name="address1"
                                        value="{{ old('address1') }}">

                                    @error('address1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address2"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Address2') }}</label>

                                <div class="col-md-6">
                                    <input id="address2" type="text"
                                        class="form-control @error('address2') is-invalid @enderror" name="address2"
                                        value="{{ old('address2') }}" required>

                                    @error('address2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address3"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Address3') }}</label>

                                <div class="col-md-6">
                                    <input id="address3" type="text"
                                        class="form-control @error('address3') is-invalid @enderror" name="address3"
                                        value="{{ old('address3') }}" required>

                                    @error('address3')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sub_cities"
                                    class="col-md-4 col-form-label text-md-right">{{ __('หมู่บ้าน') }}</label>

                                <div class="col-md-6">
                                    <input id="sub_cities" type="text"
                                        class="form-control @error('sub_cities') is-invalid @enderror" name="sub_cities"
                                        value="{{ old('sub_cities') }}" required>

                                    @error('sub_cities')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="province_id"
                                    class="col-md-4 col-form-label text-md-right">{{ __('จังหวัด') }}</label>

                                <div class="col-md-6">

                                    {!! Form::select('province_id', $provincelist, null, ['class' => 'form-control', 'id' => 'province_id']) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="city_id"
                                    class="col-md-4 col-form-label text-md-right">{{ __('อำเภอ') }}</label>

                                <div class="col-md-6">

                                    {!! Form::select('city_id', $citylist, null, ['class' => 'form-control', 'id' => 'city_id']) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="loc" class="col-md-4 col-form-label text-md-right">{{ __('Loc') }}</label>

                                <div class="col-md-6">
                                    <input id="loc" type="text" class="form-control @error('loc') is-invalid @enderror"
                                        name="loc" value="{{ old('loc') }}" required>

                                    @error('loc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="broker_color"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Broker Color') }}</label>

                                <div class="col-md-6">
                                    <input id="broker_color" type="text"
                                        class="form-control @error('broker_color') is-invalid @enderror" name="broker_color"
                                        value="{{ old('broker_color') }}" required>

                                    @error('broker_color')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
