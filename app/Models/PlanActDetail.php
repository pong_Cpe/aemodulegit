<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanActDetail extends Model
{
    use HasFactory;

    protected $table = 'plan_act_details';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'plan_act_id', 'chemical_id', 'value', 'unit_id', 'name', 'rate', 'ctype', 'use_value', 'note', 
        'createdBy', 'modifiedBy', 'act_date', 'cal_val', 'use_status', 'set_group'
    ];

    public function planact()
    {
        return $this->hasOne('App\Models\PlanAct', 'id', 'plan_act_id');
    }

    public function chemical()
    {
        return $this->hasOne('App\Models\Chemical', 'id', 'chemical_id');
    }

    public function unit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'unit_id');
    }
}
