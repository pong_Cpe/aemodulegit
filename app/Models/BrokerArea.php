<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrokerArea extends Model
{
    use HasFactory;

    protected $table = 'broker_areas';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['crop_id', 'broker_id', 'area_id'];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function area()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function copytonewcrop($fromCrop, $toCrop, $brokerList)
    {

        foreach ($brokerList as $bkey => $bvalue) {

            $chk = self::where('crop_id', $toCrop)
                ->where('broker_id', $bvalue)
                ->count();

            if ($chk == 0) {

                $baseData= self::where('crop_id', $fromCrop)
                ->where('broker_id', $bvalue)
                ->get();

                foreach ($baseData as $value) {
                    $tmp = array();
                    $tmp['crop_id'] = $toCrop;
                    $tmp['broker_id'] = $value->broker_id;
                    $tmp['area_id'] = $value->area_id;

                    self::create($tmp);
                }
            }
        }
    }
}
