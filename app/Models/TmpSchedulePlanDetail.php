<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmpSchedulePlanDetail extends Model
{
    use HasFactory;

    protected $table = 'tmp_schedule_plan_details';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'tmp_schedule_plan_id', 'chemical_id', 'value', 'unit_id', 'ctype', 
        'rate', 'set_group', 'p_value', 'p_unit_id'
    ];

    public function tmpscheduleplan()
    {
        return $this->hasOne('App\Models\TmpSchedulePlan', 'id', 'tmp_schedule_plan_id');
    }

    public function chemical()
    {
        return $this->hasOne('App\Models\Chemical', 'id', 'chemical_id');
    }

    public function unit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'unit_id');
    }

    public function punit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'p_unit_id');
    }
}
