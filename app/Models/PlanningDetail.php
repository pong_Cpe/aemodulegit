<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanningDetail extends Model
{
    use HasFactory;

    protected $table = 'planning_details';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'planning_id', 'plan_date', 'plan_value', 'area_id', 'plan_yeild', 
        'plan_areas', 'harvest_age', 'broker_id'
    ];

    public function planning()
    {
        return $this->hasOne('App\Models\Planning', 'id', 'planning_id');
    }

    public function area()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function planningdetaildates()
    {
        return $this->hasMany('App\Models\PlanningDetailDate', 'planning_detail_id');
    }
}
