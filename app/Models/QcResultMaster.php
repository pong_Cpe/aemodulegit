<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QcResultMaster extends Model
{
    use HasFactory;

    protected $table = 'qc_result_masters';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'receive_mat_detail_id', 'receive_mat_group_id', 'sub_input_item_id', 
        'code', 'name', 'note', 
        'check_weight', 'check_weight_unit_id', 
        'df_weight', 'df_weight_unit_id', 
        'random_no', 'random_no_unit', 
        'obj_weight', 'obj_weight_unit_id', 
        'rm_qa_id', 'iqf_qa_id', 'p_df_all', 'p_other_help', 'p_cri_help', 'p_all_help_laco', 
        'p_other_help_laco', 'p_cri_help_laco', 'p_qc_net', 
        'weight_grade_qc', 'weight_grade_qc_unit_id', 
        'status', 'created_by', 'modified_by'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function receivematdetail()
    {
        return $this->hasOne('App\Models\ReceiveMatDetail', 'id', 'receive_mat_detail_id');
    }

    public function subinputitem()
    {
        return $this->hasOne('App\Models\SubInputItem', 'id', 'sub_input_item_id');
    }

    public function receivematgroup()
    {
        return $this->hasOne('App\Models\ReceiveMatGroup', 'id', 'receive_mat_group_id');
    }

    public function checkweightunit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'check_weight_unit_id');
    }

    public function dfweightunit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'df_weight_unit_id');
    }

    public function randomnounit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'random_no_unit');
    }

    public function objweightunit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'obj_weight_unit_id');
    }

    public function qcgradeweightunit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'weight_grade_qc_unit_id');
    }

    public function qcresultitems()
    {
        return $this->hasMany('App\Models\QcResultItem', 'qc_result_master_id');
    }

}
