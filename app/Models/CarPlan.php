<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarPlan extends Model
{
    use HasFactory;

    protected $table = 'car_plan';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['crop_id', 'broker_id', 'plan_date', 'plan_car_est', 'plan_car_act', 'plan_note', 'start_car_leave', 'end_car_arrive', 'status', 'createdBy', 'modifiedBy'];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }
    
}
