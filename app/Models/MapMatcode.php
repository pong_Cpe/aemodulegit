<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MapMatcode extends Model
{
    use HasFactory;

    protected $table = 'map_matcode';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'input_item_id', 'broker_id', 'harvest_by', 'harvest_to', 'matcode', 'desc'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

}
