<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CropApisController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('crops', [CropApisController::class, 'index']);
Route::get('crops/{crop}', [CropApisController::class, 'show']);
Route::post('crops ', [CropApisController::class,'store']);
Route::put('crops/{crop}', [CropApisController::class, 'update']);
Route::delete('crops/{crop} ', [CropApisController::class,'destroy']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

