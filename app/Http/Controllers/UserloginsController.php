<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserAe;

class UserloginsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 25;
        $users = User::latest()->paginate($perPage);

        return view('admin.userlogins.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $useraelist = UserAe::pluck('username','id');
        return view('admin.userlogins.create',compact('useraelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $userae = UserAe::findOrFail($requestData['user_ae_id']);

        $requestData['group_id'] = $userae->group->id;

        User::create($requestData);

        return redirect('userlogins')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userlogin = User::findOrFail($id);
        return view('admin.userlogins.view', compact('userlogin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userlogin = User::findOrFail($id);
        $useraelist = UserAe::pluck('username', 'id');
        return view('admin.userlogins.edit', compact('userlogin', 'useraelist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $userae = UserAe::findOrFail($requestData['user_ae_id']);

        $requestData['group_id'] = $userae->group->id;

        $userlogin = User::findOrFail($id);
        $userlogin->update($requestData);

        return redirect('userlogins')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect('userlogins')->with('flash_message', ' deleted!');
    }
}
