<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Planning extends Model
{
    use HasFactory;

    protected $table = 'plannings';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'item_input_id', 'harvest_type_id', 'plan_date', 
        'plan_value', 'plan_kg_area', 'max_per_day', 'start_sowing'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function harvesttype()
    {
        return $this->hasOne('App\Models\HarvestType', 'id', 'harvest_type_id');
    }

    public function planningdetails()
    {
        return $this->hasMany('App\Models\PlanningDetail', 'planning_id');
    }
}
