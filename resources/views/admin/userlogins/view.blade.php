@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('User Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('userlogins.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('View User Login') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Username</th>
                                <td>{{ $userlogin->username }}</td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>{{ $userlogin->name }}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{ $userlogin->email }}</td>
                            </tr>
                            <tr>
                                <th>Group</th>
                                <td>{{ $userlogin->group->name ?? '' }}</td>
                            </tr>
                            <tr>
                                <th>Link</th>
                                <td>{{ $userlogin->userae->username ?? '' }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary"
                                        href="{{ route('userlogins.edit', $userlogin->id) }}">Edit</a>
                                    <form action="{{ route('userlogins.destroy', $userlogin->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
