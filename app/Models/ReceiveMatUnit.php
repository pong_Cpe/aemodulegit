<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceiveMatUnit extends Model
{
    use HasFactory;

    protected $table = 'receive_mat_units';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'name', 'short_name', 'diff_weight', 'note', 'status'
    ];

}
