<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFarmer extends Model
{
    use HasFactory;

    protected $table = 'user_farmer';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'user_id', 'manager_id', 'review_id', 'broker_id', 'area_id', 'farmer_id', 
        'head_id', 'sowing_city', 'farmer_code', 'status', 'createdBy', 'modifiedBy'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function manager()
    {
        return $this->hasOne('App\Models\User', 'id', 'manager_id');
    }

    public function review()
    {
        return $this->hasOne('App\Models\User', 'id', 'review_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function area()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function farmer()
    {
        return $this->hasOne('App\Models\Farmer', 'id', 'farmer_id');
    }

    public function head()
    {
        return $this->hasOne('App\Models\Head', 'id', 'head_id');
    }
}
