<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanSchedule extends Model
{
    use HasFactory;

    protected $table = 'plan_schedules';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'input_item_id', 'code', 'main_code', 'name', 'gapdata', 'details', 'day', 
         'area_id', 'priority', 'broker_id'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function area()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function planscheduledetails()
    {
        return $this->hasMany('App\Models\PlanScheduleDetail', 'plan_schedule_id');
    }
}
