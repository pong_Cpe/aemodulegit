<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaRegionMapping extends Model
{
    use HasFactory;

    protected $table = 'qa_region_mapping';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'qa_rate_set_id', 'name', 'mapping', 'viewable'
    ];

    public function qarateset()
    {
        return $this->hasOne('App\Models\QaRateSet', 'id', 'qa_rate_set_id');
    }
    
}
