@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Crop Management') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><a class="btn btn-success" href="{{ route('crops.create') }}"> Create New
                            Crop</a></div>

                    <div class="card-body">
                        <div class="pull-right">

                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>SAP CODE</th>
                                <th>Name</th>
                                <th>Link</th>
                                <th>Timeline</th>
                                <th></th>
                            </tr>
                            @foreach ($crops as $crop)
                                <tr>
                                    <td>{{ $crop->sap_code }}</td>
                                    <td>{{ $crop->name }}</td>
                                    <td>{{ $crop->linkurl }}</td>                                    
                                    <td>{{ $crop->startdate }} / {{ $crop->enddate }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('crops.show', $crop->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('crops.edit', $crop->id) }}">Edit</a>
                                        <form action="{{ route('crops.destroy', $crop->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
