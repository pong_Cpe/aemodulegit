<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChemPlan extends Model
{
    use HasFactory;

    protected $table = 'chem_plan';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['crop_id', 'input_item_id', 'chem_package_id', 'value'];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function chempackage()
    {
        return $this->hasOne('App\Models\ChemPackage', 'id', 'chem_package_id');
    }
}
