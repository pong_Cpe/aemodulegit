<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Yeild extends Model
{
    use HasFactory;

    protected $table = 'yeilds';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'area_id', 'broker_id', 'input_item_id', 'harvest_type_id', 'start_date', 
        'end_date', 'rate', 'status', 'kg_per_area'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function area()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function harvesttype()
    {
        return $this->hasOne('App\Models\HarvestType', 'id', 'harvest_type_id');
    }
}
