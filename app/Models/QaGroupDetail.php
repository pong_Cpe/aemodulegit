<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaGroupDetail extends Model
{
    use HasFactory;

    protected $table = 'qa_group_details';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'qa_master_id', 'sowing_id', 'sample_plan_id', 'note', 'status', 'createdBy', 'modifiedBy'];

    public function qamaster()
    {
        return $this->hasOne('App\Models\QaMaster', 'id', 'qa_master_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }

    public function sampleplan()
    {
        return $this->hasOne('App\Models\SamplePlan', 'id', 'sample_plan_id');
    }


}
