<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HarvestPlan extends Model
{
    use HasFactory;

    protected $table = 'harvest_plans';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'area_id', 'broker_id', 'farmer_id', 'sowing_id', 'input_item_id', 
        'user_id', 'head_id', 'seed_code_id', 'age', 'harvest_type', 'havest_by', 
        'ref_id', 'mat_type', 'delivery_status', 'date_plan', 'date_est', 'date_act', 
        'value_est', 'value_bf_harvest', 'value_act', 'note', 'reject_status', 'reject_note', 
        'qa_grade', 'run_no', 'selected_location', 'createdBy', 'modifiedBy'];


    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function area()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function broker()
    {
        return $this->hasOne('App\Models\Broker', 'id', 'broker_id');
    }

    public function farmer()
    {
        return $this->hasOne('App\Models\Farmer', 'id', 'farmer_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }

    public function inputitem()
    {
        return $this->hasOne('App\Models\InputItem', 'id', 'input_item_id');
    }

    public function ae()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function aesup()
    {
        return $this->hasOne('App\Models\User', 'id', 'head_id');
    }

    public function seedcode()
    {
        return $this->hasOne('App\Models\SeedCode', 'id', 'seed_code_id');
    }

    public function harvestplanref()
    {
        return $this->hasOne('App\Models\HarvestPlan', 'id', 'ref_id');
    }

    public function createby()
    {
        return $this->hasOne('App\Models\User', 'id', 'createdBy');
    }

    public function updateby()
    {
        return $this->hasOne('App\Models\User', 'id', 'modifiedBy');
    }
}
