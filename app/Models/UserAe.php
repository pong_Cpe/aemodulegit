<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAe extends Model
{
    use HasFactory;

    protected $table = 'users';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'username', 'password', 'email', 'init', 'fname', 'lname', 'citizenid', 'group_id', 
        'canEdit', 'reviewteam', 'status'
    ];

    public function group()
    {
        return $this->hasOne('App\Models\Group', 'id', 'group_id');
    }
}
