@extends('templates.mazer')

@section('content')

    <div class="page-heading">
        <h3>{{ __('User Management') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><a class="btn btn-success" href="{{ route('userlogins.create') }}"> Create
                            New Users</a></div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Link</th>
                                <th></th>
                            </tr>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->group->name ?? '' }}</td>
                                    <td>{{ $user->userae->username ?? '' }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('userlogins.show', $user->id) }}">Show</a>
                                        <a class="btn btn-primary"
                                            href="{{ route('userlogins.edit', $user->id) }}">Edit</a>
                                        <form action="{{ route('userlogins.destroy', $user->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
