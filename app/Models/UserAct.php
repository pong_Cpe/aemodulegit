<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAct extends Model
{
    use HasFactory;

    protected $table = 'user_acts';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'plan_act_id', 'sowing_id', 'user_id', 'act_date', 'act_details', 'age_code', 
        'growing_rate', 'weed_rate', 'moisture_rate', 'disease1', 'disease2', 'disease3', 'disease4', 
        'disease5', 'disease6', 'disease7', 'disease8', 'disease9', 'disease_other', 'disease_other_text', 
        'insect1', 'insect2', 'insect3', 'insect4', 'insect5', 'insect6', 'insect7', 'insect8', 
        'insect9', 'insect_other', 'insect_other_text', 'harvest', 'labor', 'sorting', 'volumn', 
        'disguise', 'harvest_other', 'follow_up', 'created', 'modified', 'follow_case', 'attach_dir', 
        'attach', 'img_date', 'review_id', 'spray_flag', 'spray_note', 'spray_gallon', 'spray_bodo', 
        'spray_head', 'spray_machine', 'spray_team'
    ];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function planact()
    {
        return $this->hasOne('App\Models\PlanAct', 'id', 'plan_act_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function review()
    {
        return $this->hasOne('App\Models\User', 'id', 'review_id');
    }
}
