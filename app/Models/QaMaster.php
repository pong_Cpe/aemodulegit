<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaMaster extends Model
{
    use HasFactory;

    protected $table = 'qa_masters';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'crop_id', 'qa_code', 'plan_date', 'receive_sample_date', 'test_date', 'result_date', 'summary_result', 'result_qa', 'grade', 'sample_no', 'note', 'test_no', 'ref_id', 'retest_flag', 'loc_type', 'createdBy', 'modifiedBy'];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function prevqamaster()
    {
        return $this->hasOne('App\Models\QaMaster', 'id', 'ref_id');
    }

    public function qagroupdetails()
    {
        return $this->hasMany('App\Models\QaGroupDetail', 'qa_master_id');
    }

    public function qaitemdetails()
    {
        return $this->hasMany('App\Models\QaItemDetail', 'qa_master_id');
    }
}
