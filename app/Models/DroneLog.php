<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DroneLog extends Model
{
    use HasFactory;

    protected $table = 'drone_logs';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = ['crop_id', 'sowing_id', 'fly_date', 'name', 'link', 'note'];

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id', 'crop_id');
    }

    public function sowing()
    {
        return $this->hasOne('App\Models\Sowing', 'id', 'sowing_id');
    }
}
